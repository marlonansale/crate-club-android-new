package com.crateclub.crateclub

class AppConstant {
    companion object {
        val APP_NAME = "CRATE CLUB"
        val URL_BASE = "https://crateclub.us"
        val URL_NON_SECURE = "$URL_BASE/wp-json/crate-club/v1"
        val PER_PAGE = 20

        val APP_FAQ_URL = "https://www.crateclub.us/faq/"

        val kYoutubeUrl = "https://www.youtube.com/channel/UCcOT370bAtA1nFXcwBA9GCA"
        val kInstagramUrl = "https://www.instagram.com/officialcrateclub/"
        val kFacebookUrl = "https://www.facebook.com/crateclubofficial/"

        val kSupportEmail = "support@crateclub.us"

        var messageMaintenance = ""
        var appMaintenance = false

        val appBuild = 1
    }
}