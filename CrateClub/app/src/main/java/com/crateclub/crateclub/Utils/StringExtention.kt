package com.crateclub.crateclub.Utils

import com.crateclub.crateclub.Model.ArticleModel

class StringExtention {
    companion object {
         fun htmlArticle(articleModel: ArticleModel) : String {

            var    htmlContent = articleModel.getContent()
            htmlContent = htmlContent.replace("font-family","")
            htmlContent = htmlContent.replace("font-size","")
            htmlContent = htmlContent.replace("width:","")
            htmlContent = htmlContent.replace("width=","")
            htmlContent = htmlContent.replace("height:","")
            htmlContent = htmlContent.replace("sizes=","")
            htmlContent = htmlContent.replace("class=","")
            htmlContent = htmlContent.replace("style=","")
            htmlContent = htmlContent.replace("srcset=","")
            htmlContent = htmlContent.replace("id=","")
            htmlContent = htmlContent.replace("data-orig-size=","")
            htmlContent = htmlContent.replace("data-orig-file=","")
            htmlContent = htmlContent.replace("data-large-file=","")
            htmlContent = htmlContent.replace("data-medium-file=","")
            htmlContent = htmlContent.replace("data-medium-file=","")
            htmlContent = htmlContent.replace("“","")

            htmlContent = htmlContent.replace("[gallery type=\"rectangular\" link=\"file\" ids=\"30598,30599,30600,30601,30602,30603,30604,30605,30606,30607,30608,30609,30610,30611,30612,30613,30614,30615,30616,30617,30618,30619,30620,30621,30622,30623,30624,30625,30626,30627,30628,30629,30630,30631,30632,30633,30634,30635,30636,30637,30638,30639,30640\"]","")




            htmlContent = "<!DOCTYPE html>" +
                    "<head><link rel=\"stylesheet\" type=\"text/css\" href=\"content.css\">" +
                    " <meta charset=\"utf-8\" name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />" +
                    "</head>" +
                    "<body >" +
                    "<div style='height:auto; overflow: scroll;' id='divContainer'><p class='title'>${articleModel.getTitle()}</p>" +
                    "<img src='${articleModel.getFeatureImage()}' class='imgFeatured'  >" +
                    "<p class='subheadAuthor'>${articleModel.getAuthorName()}</p><p class='subheadDate'>${articleModel.getDate()}</p>" +
                    "<div class='divContent'>$htmlContent</div></div></body></html>"



            return htmlContent

        }
    }
}