package com.crateclub.crateclub

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.ActionBar
import android.util.Log
import android.view.View
import com.crateclub.crateclub.Service.ApiResult
import com.crateclub.crateclub.Service.UrlService
import com.crateclub.crateclub.Utils.AlertUtils
import kotlinx.android.synthetic.main.nav_bar_layout.*
import org.json.JSONObject

class MainActivity : RootActivity(), ApiResult.AppReceiver {

    lateinit var  resultReceiver: ApiResult
    private var intentService : Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.nav_bar_layout)

        btnMenuLeft.visibility  = View.GONE
        resultReceiver = ApiResult(Handler(), this)

        if (this.isNetworkAvailable()){
            intentService = Intent(this@MainActivity, UrlService::class.java)
            intentService!!.putExtra("url","${AppConstant.URL_NON_SECURE}/appinfo")
            intentService!!.putExtra("receiver", resultReceiver)
            startService(intentService!!)

        }else {
            AlertUtils.noInternetDialog(this@MainActivity)

        }
    }


    override fun onReceiveResult(resultCode: Int, resultData: Bundle){

        if(resultCode == 200){
            val json = JSONObject(resultData.getString("resultString"))

            AppConstant.appMaintenance =  json.getBoolean("app_maintenance")
            AppConstant.messageMaintenance = json.getString("app_maintenance_message")

            val build = json.getInt("android_app_build")

            if(build > AppConstant.appBuild){
                val alertDialogBuilder = android.app.AlertDialog.Builder(this)

                // set title
                alertDialogBuilder.setTitle("A new version of the app is now available")

                // set dialog message
                alertDialogBuilder
                    .setMessage("In order to continue, please update your Crate Club app now. This should only take a few moments.")
                    .setCancelable(false)
                    .setPositiveButton("OK") {
                            dialog, id ->
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.crateclub.crateclub")))
                    }

                // create alert dialog
                val alertDialog = alertDialogBuilder.create()
                // show it
                alertDialog.show()
            }else{
                val intent = Intent(this, GuestActivity::class.java)
                startActivity(intent)
            }

            stopService(intentService!!)
        }

    }


}
