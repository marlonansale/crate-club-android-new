package com.crateclub.crateclub.Service

import android.app.IntentService
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import okhttp3.*
import java.io.IOException

class UrlService : IntentService("UrlService") {

    private val client = OkHttpClient()
    lateinit var resultReceiver: ResultReceiver

    override fun onHandleIntent(intent: Intent?) {
        resultReceiver = intent!!.getParcelableExtra("receiver")
        val url = intent.getStringExtra("url")
        run(url)
    }

    fun run(url:String) {

        val request = Request.Builder()
            .url(url)
            .get()
            .build()


        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                val bundle = Bundle()
                bundle.putString("message","error")
                resultReceiver.send(400,bundle)
            }
            override fun onResponse(call: Call, response: Response) {
                if (response.code() == 200){
                    val bundle = Bundle()
                    bundle.putString("resultString",response.body()!!.string())
                    resultReceiver.send(200,bundle)
                }else{
                    val bundle = Bundle()
                    bundle.putString("message","error")
                    resultReceiver.send(400,bundle)
                }
            }
        })

    }

}
