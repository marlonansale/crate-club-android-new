package com.crateclub.crateclub.Utils

import android.app.AlertDialog
import android.content.Context

object AlertUtils {

    fun errorDialog(context: Context, message: String){
        showDialog(context,"Error",message)
    }

    fun noInternetDialog(context: Context){
        showDialog(context,"Alert","No internet Connection. Please try again.")
    }

    fun showDialog(context: Context, title: String, message: String) {
                val builder = AlertDialog.Builder(context)
                builder.setTitle(title)
                builder.setMessage(message)
                builder.setPositiveButton(android.R.string.ok, null)
                builder.show()

    }



}