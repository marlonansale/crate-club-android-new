package com.crateclub.crateclub.ViewHolder

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.crateclub.crateclub.R

class KnowledgeViewHolder (view: View): RecyclerView.ViewHolder(view) {
    var txtViewTitle :TextView = view.findViewById(R.id.txtViewTitle)

    var txtViewDetail:TextView  = view.findViewById(R.id.txtViewDetail)
    var txtViewAuthor:TextView  = view.findViewById(R.id.txtViewAuthor)
    var txtViewDate:TextView  = view.findViewById(R.id.txtViewDate)

    var imgViewKnowledge: ImageView = view.findViewById(R.id.imgViewKnowledge)
    var mainView: RelativeLayout = view.findViewById(R.id.mainView)

    var cardView: CardView = view.findViewById(R.id.cardView)

}