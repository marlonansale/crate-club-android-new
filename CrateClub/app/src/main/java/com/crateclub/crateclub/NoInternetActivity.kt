package com.crateclub.crateclub

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class NoInternetActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_no_internet)
    }
}
