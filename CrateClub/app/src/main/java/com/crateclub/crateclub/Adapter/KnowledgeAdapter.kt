package com.crateclub.crateclub.Adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crateclub.crateclub.Model.ArticleModel
import com.crateclub.crateclub.R
import com.crateclub.crateclub.ViewHolder.KnowledgeViewHolder
import java.util.ArrayList
import com.bumptech.glide.Glide
import com.crateclub.crateclub.GuestActivity

class KnowledgeAdapter(private var items : ArrayList<ArticleModel>, val context: Context) : RecyclerView.Adapter<KnowledgeViewHolder>(), View.OnClickListener {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KnowledgeViewHolder {

        return KnowledgeViewHolder(LayoutInflater.from(context).inflate(R.layout.view_holder_knowledge, parent, false))
    }

    override fun getItemCount(): Int {
        return  items.size
    }

    override fun onBindViewHolder(holder: KnowledgeViewHolder, position: Int) {

        val articleModel = items[position]

        holder.txtViewTitle.text = articleModel.getTitle()
        holder.txtViewAuthor.text = articleModel.getAuthorName()
        holder.txtViewDate.text = articleModel.getDate()
        holder.txtViewDetail.text = articleModel.getExcerpt()

        Glide.with(context).load(articleModel.getThumbImage()).into(holder.imgViewKnowledge)
        //GlideApp.with(context).load(articleModel.getThumbImage()).into(holder.imgView)

        holder.cardView.tag = position
        holder.cardView.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        val position = v!!.tag as Int
        val articleModel = items[position]

        val guestActivity = context as GuestActivity
        guestActivity.pushArticle(articleModel)
    }
}