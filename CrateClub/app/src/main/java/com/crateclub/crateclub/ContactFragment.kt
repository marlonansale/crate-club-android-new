package com.crateclub.crateclub


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_contact.*


class ContactFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btnContact.setOnClickListener {

            val i = Intent(Intent.ACTION_SEND)
            i.type = "text/plain"
            i.putExtra(Intent.EXTRA_EMAIL, arrayOf(AppConstant.kSupportEmail))

            try {
                startActivity(Intent.createChooser(i, "Send mail..."))
            } catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(context!!, "There are no email clients installed.", Toast.LENGTH_SHORT).show()
            }
        }

        btnFaq.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(AppConstant.APP_FAQ_URL))
            startActivity(intent)
        }


        imgBtnYT.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(AppConstant.kYoutubeUrl))
            startActivity(intent)
        }

        imgBtnFB.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(AppConstant.kFacebookUrl))
            startActivity(intent)
        }

        imgBtnIns.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(AppConstant.kInstagramUrl))
            startActivity(intent)
        }

    }

}
