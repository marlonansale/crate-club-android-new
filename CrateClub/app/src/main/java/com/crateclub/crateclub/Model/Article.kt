package com.crateclub.crateclub.Model

import android.os.Build
import android.text.Html
import org.json.JSONObject
import java.io.Serializable
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class Article (jsonObject: Any) : Serializable {

    var content:String? = null
    var excerpt:String? = null
    var title:String? = null
    var link: String? = null

    var category:String? = null
    var categoryID:Int = 0

    var author:String? = null
    var authorImage:String? = null
    var authorDescription:String? = null
    var authorID:Int = 0

    var imgThumb:String? = null
    var imgCat:String? = null

    var imgFull:String? = null

    var date: Date? = null
    var articleID:Int = 0

    init {
        try {

            if (jsonObject is JSONObject) {
                this.articleID = jsonObject.getString("id") .toInt()

                if (jsonObject.has("title")) {
                    val titleDict = jsonObject.getJSONObject("title")
                    this.title =  titleDict.getString("rendered")

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        this.title =  Html.fromHtml(this.title, Html.FROM_HTML_MODE_LEGACY).toString()
                    } else {
                        this.title = Html.fromHtml(this.title).toString()
                    }

                }

                if (jsonObject.has("excerpt")) {
                    val excerptDict = jsonObject.getJSONObject("excerpt")
                    this.excerpt =  excerptDict.getString("rendered")

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        this.excerpt =  Html.fromHtml(this.excerpt, Html.FROM_HTML_MODE_LEGACY).toString()
                    } else {
                        this.excerpt = Html.fromHtml(this.excerpt).toString()
                    }

                }

                if(jsonObject.has("author_details")){
                    val author = jsonObject.getJSONObject("author_details")

                    this.author  = author.getString("author_name")
                    this.authorDescription = author.getString("author_description")

                    this.authorImage = author.getString("author_avatar")
                }

                this.link = jsonObject.getString("link")

                if (jsonObject.has("featured_images")){
                    val jsonImg = jsonObject.getJSONObject("featured_images")

                    this.imgThumb  = jsonImg.getString("post-thumbnail")
                    this.imgCat  =   jsonImg.getString("cat-feat-img")
                    this.imgFull  =  jsonImg.getString("full")

                }

                val strDate = jsonObject.getString("date")


                val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
                this.date = formatter.parse(strDate)
                this.authorID = jsonObject.getInt("author")

                if (jsonObject.has("content")){
                    val content = jsonObject.getJSONObject("content")

                    this.content = content.getString("rendered")
                }

                val arrCat =  jsonObject.getJSONArray("categories")
                if (arrCat.length() > 0) {
                    this.categoryID = arrCat.getInt(0)
                }

                if (jsonObject.has("category_details")){
                    val categoryObject = jsonObject.getJSONObject("category_details")
                    this.category  = categoryObject.getString("category_name")

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        this.category =  Html.fromHtml(this.category, Html.FROM_HTML_MODE_LEGACY).toString()
                    } else {
                        this.category = Html.fromHtml(this.category).toString()
                    }

                }

            }

        }catch (e : Exception){

        }

    }

}