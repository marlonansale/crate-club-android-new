package com.crateclub.crateclub

import android.annotation.SuppressLint
import android.net.http.SslError
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.util.Log
import android.view.View
import android.webkit.*
import com.crateclub.crateclub.Utils.AlertUtils
import kotlinx.android.synthetic.main.activity_website.*
import kotlinx.android.synthetic.main.nav_bar_layout.*

class WebsiteActivity : RootActivity() {

    var hasLoad = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_website)

        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.nav_bar_layout)

        if (!hasLoad){
            hasLoad = true
            setWebData()
        }

        btnMenuLeft.setOnClickListener {
            finish()
        }

    }



    @SuppressLint("SetJavaScriptEnabled")
    fun setWebData(){
        if (isNetworkAvailable()){


            val url = intent.getStringExtra("url")

            Log.d("urlWeb",url)

            webView.webViewClient = WebViewClient()
            webView.settings.javaScriptEnabled = true
            webView.webViewClient = WebViewClient()
            webView.loadUrl(url)



            this.webView.webChromeClient = object : WebChromeClient() {

                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                    super.onProgressChanged(view, newProgress)

                    if (newProgress > 50){
                        progressBar.visibility = View.GONE
                    }
                }

            }

        }else{
            AlertUtils.noInternetDialog(this)
            finish()
        }
    }
}
