package com.crateclub.crateclub

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crateclub.crateclub.Adapter.KnowledgeAdapter
import com.crateclub.crateclub.Model.ArticleModel
import kotlinx.android.synthetic.main.fragment_knowledge_list.*


class KnowledgeListFragment : Fragment() {

    var arrKnowledge = ArrayList<ArticleModel>()
    lateinit var adapter:KnowledgeAdapter



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_knowledge_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

     fun loadArticle(){
        adapter = KnowledgeAdapter(arrKnowledge,context!!)
        listKnowledge.adapter = adapter
        listKnowledge.layoutManager =  LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL,false)

    }
}
