package com.crateclub.crateclub

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.ActionBar
import android.view.View
import com.crateclub.crateclub.Model.ArticleModel
import com.crateclub.crateclub.Service.ApiResult
import com.crateclub.crateclub.Service.UrlService
import com.crateclub.crateclub.Utils.AlertUtils
import kotlinx.android.synthetic.main.activity_guest.*
import kotlinx.android.synthetic.main.nav_bar_layout.*
import org.json.JSONArray

class GuestActivity : RootActivity(), ApiResult.AppReceiver  {

    private val knowledgeListFragment = KnowledgeListFragment()
    private val contactFragment = ContactFragment()
    private var intentService : Intent? = null

    var knowledgeDetailFragment:KnowledgeDetailFragment? = null
    var selected = 0

    lateinit var  resultReceiver: ApiResult

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guest)

        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.nav_bar_layout)

        supportFragmentManager.beginTransaction().add(R.id.container_knowledge, knowledgeListFragment).commit()
        supportFragmentManager.beginTransaction().add(R.id.container_contact, contactFragment).commit()


        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        btnMenuLeft.visibility = View.GONE
        container_contact.visibility = View.GONE

        txtHead.text = "KNOWLEDGE"

        //URL_MAIN+"/wp-json/wp/v2/posts?page=\(page)&per_page=\(PER_PAGE)"
        resultReceiver = ApiResult(Handler(), this)

        if(this.isNetworkAvailable()){
            intentService = Intent(this@GuestActivity, UrlService::class.java)
            intentService!!.putExtra("url","${AppConstant.URL_BASE}/wp-json/wp/v2/posts?page=1&per_page=${AppConstant.PER_PAGE}")
            intentService!!.putExtra("receiver", resultReceiver)
            startService(intentService)
        }else{
            AlertUtils.noInternetDialog(this)
        }

        btnMenuLeft.setOnClickListener {
            removeDetail()
        }

        txtMaintenance.text =   AppConstant.messageMaintenance
    }

    override fun onReceiveResult(resultCode: Int, resultData: Bundle){

        if(resultCode == 200){
            val jsonArray = JSONArray(resultData.getString("resultString"))

            val arrData : ArrayList<ArticleModel> = (0..(jsonArray.length() - 1))
                .map { jsonArray.getJSONObject(it) }
                .mapTo(ArrayList()) {

                    ArticleModel(it)

                }

            progressBar.visibility = View.GONE
            this@GuestActivity.knowledgeListFragment.arrKnowledge = arrData
            this@GuestActivity.knowledgeListFragment.loadArticle()

            stopService(intentService)
        }
    }


    fun pushArticle(articleModel: ArticleModel){
        knowledgeListFragment.view!!.visibility = View.INVISIBLE
        knowledgeDetailFragment = KnowledgeDetailFragment()

        val bundle = Bundle()
        bundle.putSerializable("article",articleModel)
        knowledgeDetailFragment!!.arguments = bundle

        supportFragmentManager.beginTransaction().add(R.id.container_knowledge, knowledgeDetailFragment!!).commit()

        btnMenuLeft.visibility = View.VISIBLE
    }




    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        container_knowledge.visibility = View.GONE
        container_contact.visibility = View.GONE

        when (item.itemId) {
            R.id.navigation_home -> {
                container_knowledge.visibility = View.VISIBLE
                selected = 0
                txtHead.text = "KNOWLEDGE"

                if (this.knowledgeDetailFragment != null){
                    btnMenuLeft.visibility = View.VISIBLE
                }else{
                    btnMenuLeft.visibility = View.GONE
                }

                return@OnNavigationItemSelectedListener true

            }
            R.id.navigation_contact -> {
                btnMenuLeft.visibility = View.GONE
                container_contact.visibility = View.VISIBLE
                selected = 1
                txtHead.text = "CONTACT US"
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    override fun onBackPressed() {

        if (selected == 1){
            val view :View = navigation.findViewById(R.id.navigation_home)
            view.performClick()
        }else{

            if (this.knowledgeDetailFragment != null){
                removeDetail()
            }else{
                super.onBackPressed()
            }

        }
    }

    fun removeDetail(){
        supportFragmentManager.beginTransaction().remove(knowledgeDetailFragment!!).commit()
        knowledgeDetailFragment = null
        knowledgeListFragment.view!!.visibility = View.VISIBLE
        btnMenuLeft.visibility = View.GONE
    }
}
