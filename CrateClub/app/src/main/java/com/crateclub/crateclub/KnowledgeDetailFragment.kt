package com.crateclub.crateclub


import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import com.crateclub.crateclub.Model.ArticleModel
import kotlinx.android.synthetic.main.fragment_knowledge_detail.*


class KnowledgeDetailFragment : Fragment() {

    var articleModel:ArticleModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_knowledge_detail, container, false)
    }


    @SuppressLint("SetJavaScriptEnabled")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        webViewDetail.settings.javaScriptEnabled = true

        articleModel = arguments!!.getSerializable("article") as ArticleModel
        setData()
    }

    fun setData(){


        if (this.articleModel != null){

            this.webViewDetail.loadDataWithBaseURL("file:///android_asset/", this.articleModel!!.getContentHtml(), "text/html", "UTF-8", null)

            this.webViewDetail.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW

            this.webViewDetail.setBackgroundColor(Color.TRANSPARENT)

            this.webViewDetail.setLayerType(WebView.LAYER_TYPE_HARDWARE, null)
            //webViewContent.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            this.webViewDetail.settings.cacheMode = WebSettings.LOAD_NO_CACHE

            this.webViewDetail.setBackgroundColor(Color.TRANSPARENT)
            webViewDetail.isVerticalScrollBarEnabled = false
            webViewDetail.isHorizontalScrollBarEnabled = false

            webViewDetail.webViewClient = object : WebViewClient() {

                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                    val strWeb = request!!.url.toString()

                    val webSite = Intent(context,WebsiteActivity::class.java)
                    webSite.putExtra("url",strWeb)
                    context!!.startActivity(webSite)

                    return true
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)

                }


            }


        }

    }
}
