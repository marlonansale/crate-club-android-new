package com.crateclub.crateclub.Model

import com.crateclub.crateclub.AppConstant
import com.crateclub.crateclub.Utils.StringExtention
import java.io.Serializable
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*


class ArticleModel (jsonObject: Any) : Serializable {
    private var article: Article? = null


    init {
//        if (jsonObject is BookMarked){
//            bookMarked = jsonObject
//        }else{
        this.article = Article(jsonObject)
//        }

    }

    fun getTitle(): String {

//        if (bookMarked != null){
//            return bookMarked!!.title!!

        if (article!!.title != null) {
            return article!!.title!!
        } else {
            return AppConstant.APP_NAME
        }

    }


    fun getAuthorID(): Int {


        return article!!.authorID


    }


    fun getAuthorName(): String {


        var authorName = AppConstant.APP_NAME

        if (article!!.author != null) {
            authorName = article!!.author!!
        }

        return authorName

    }


    fun getAuthorDesc(): String {
        return article!!.authorDescription!!
    }


    fun getAuthorAvatar(): String {
        return article!!.authorImage!!
    }

    fun getDateData(): Date {

        return article!!.date!!

    }

    fun getDate(): String {
        val formatter = SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault())

        return formatter.format(getDateData())
    }

    fun getExcerpt():String {
        return  article!!.excerpt!!
    }

    fun getArticleID(): Int {


        return article!!.articleID


    }


    fun getThumbImage(): String {

        return if (article != null && article!!.imgThumb != null) {
            article!!.imgThumb!!
        } else {
            "https://lexiconcss.wedeploy.io/images/thumbnail_placeholder.gif"
        }


    }

    fun getCategoryName(): String {
        if (article!!.category != null) {
            return article!!.category!!
        } else {
            return AppConstant.APP_NAME
        }
    }


    fun getFeatureImage(): String {
        return if (article!!.imgCat != null) {
            article!!.imgCat!!
        } else {
            "https://lexiconcss.wedeploy.io/images/thumbnail_placeholder.gif"
        }
    }

    fun getContent():String{
        return article!!.content!!
    }

    fun getContentHtml():String {
        return StringExtention.htmlArticle(this)
    }


    fun getLink(): String {
        return article!!.link!!
    }
}


//    fun isBookMarked():Boolean{
//        val faveList = LitePal.where("articleID = ? ", getArticleID().toString()).find(BookMarked::class.java)
//        return faveList.size >0
//    }
//
//    fun unBookMarked(){
//
//        val faveList = LitePal.where("articleID = ? ", getArticleID().toString()).find(BookMarked::class.java)
//
//        //val faveList = DataSupport.where("articleID = ${getArticleID()}").find<BookMarked>(BookMarked::class.java)
//
//        if(!faveList.isEmpty()){
//            val fave = faveList[0]
//            fave.delete()
//        }
//
//    }
//
//    fun bookMarked(){
//        val fave = BookMarked()
//        fave.author = getAuthorName()
//        fave.articleID = getArticleID()
//        fave.title = getTitle()
//        fave.date = article!!.date
//        fave.imgThumb = getThumbImage()
//        fave.dateAdded = Date()
//        fave.save()
//    }

