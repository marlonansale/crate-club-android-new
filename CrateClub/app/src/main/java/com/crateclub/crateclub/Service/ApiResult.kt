package com.crateclub.crateclub.Service

import android.os.Handler
import android.os.ResultReceiver
import android.os.Bundle


class ApiResult(
    handler: Handler,

    private val appReceiver: AppReceiver?
) : ResultReceiver(handler) {

    override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
        appReceiver?.onReceiveResult(resultCode, resultData)
    }


    interface AppReceiver {
        fun onReceiveResult(resultCode: Int, resultData: Bundle)
    }
}