package crateclub.crateclub.com.crateclubnew

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import okhttp3.*
import java.io.IOException
import org.json.JSONObject



class MainActivity : AppCompatActivity() {
    private val client = OkHttpClient()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intent = Intent(this,GuestActivity::class.java)
        startActivity(intent)


        run("")
    }

    fun run(url: String) {
        val request = Request.Builder()
            .url(url)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {}
            override fun onResponse(call: Call, response: Response) {
                val responseData = response.body()!!.string()
                val json = JSONObject(responseData)
            }
        })
    }

}
