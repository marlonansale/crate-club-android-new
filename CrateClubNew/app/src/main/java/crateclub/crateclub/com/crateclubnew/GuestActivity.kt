package crateclub.crateclub.com.crateclubnew

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.ActionBar
import android.view.View
import kotlinx.android.synthetic.main.activity_guest.*
import kotlinx.android.synthetic.main.nav_bar_layout.*

class GuestActivity : AppCompatActivity() {

    private val knowledgeListFragment = KnowledgeListFragment()
    private val contactFragment = ContactFragment()
    var selected = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guest)

        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.nav_bar_layout)

        supportFragmentManager.beginTransaction().add(R.id.container_knowledge, knowledgeListFragment).commit()
        supportFragmentManager.beginTransaction().add(R.id.container_contact, contactFragment).commit()

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        btnMenuLeft.visibility = View.GONE

        container_contact.visibility = View.GONE

        txtHead.text = "KNOWLEDGE"
    }


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        container_knowledge.visibility = View.GONE
        container_contact.visibility = View.GONE

        when (item.itemId) {
            R.id.navigation_home -> {
                container_knowledge.visibility = View.VISIBLE
                selected = 0
                txtHead.text = "KNOWLEDGE"
                return@OnNavigationItemSelectedListener true

            }
            R.id.navigation_contact -> {
                container_contact.visibility = View.VISIBLE
                selected = 1
                txtHead.text = "CONTACT US"
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


}
